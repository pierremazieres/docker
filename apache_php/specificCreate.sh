#!/usr/bin/env bash
#set -xe
# WRITE SPECIFIC CREATE INSTRUCTIONS HERE
# sample files can be found in archive : sample.tar.gz
# they contains folders :
# - configuration : default configuration
# - html : default index and test site
# log : will be created if not existing
ENVIRONMENT_DIRECTORY_FULL_PATH=${DIRECTORY_FULL_PATH}/${ENVIRONMENT}
docker create \
    --name ${CONTAINER} \
    -v ${ENVIRONMENT_DIRECTORY_FULL_PATH}/html:/var/www/html \
    -v ${ENVIRONMENT_DIRECTORY_FULL_PATH}/configuration:/etc/apache2 \
    -v ${ENVIRONMENT_DIRECTORY_FULL_PATH}/log:/var/log/apache2 \
    -p ${HTTP_PORT}:80 \
    ${IMAGE}
