#!/usr/bin/env bash
#set -xe
# create & fill docker file
cp Dockerfile.pattern Dockerfile
sed -i s/"{ENVIRONMENT}"/${ENVIRONMENT}/g Dockerfile
# run specific build (if needed)
SPECIFIC_BUILD_SCRIPT=${DIRECTORY_FULL_PATH}/specificBuild.sh
if [[ -x ${SPECIFIC_BUILD_SCRIPT} ]]; then
    ${SPECIFIC_BUILD_SCRIPT}
fi
# build image
sudo docker build -t ${IMAGE} .
# run specific clean (if needed)
SPECIFIC_CLEAN_SCRIPT=${DIRECTORY_FULL_PATH}/specificClean.sh
if [[ -x ${SPECIFIC_CLEAN_SCRIPT} ]]; then
    ${SPECIFIC_CLEAN_SCRIPT}
fi
# remove docker file
rm Dockerfile
