#!/usr/bin/env bash
#set -xe
# set inputs
SCRIPT_FULL_PATH=$1
export ENVIRONMENT=$2
# stop if environment is not provided
if [[ ${ENVIRONMENT} == "" ]]; then
    echo "ERROR : You must provived an environment (i.e. : bare, dev(development), stg(staging), prod(production), ci(continuous integration) ...)"
    exit 1
fi
# set global configuration
export DIRECTORY_FULL_PATH=`dirname "${SCRIPT_FULL_PATH}"`
# 1 : "//" is for global find & replace
# 2 : '/' is character to find
# 3 : '/' is to separate find & replace strings
# 4 : ' ' is the caracter to replace with
SPLITTED_DIRECTORY_FULL_PATH=(${DIRECTORY_FULL_PATH//// })
DOCKER_ROOT_NAME=${SPLITTED_DIRECTORY_FULL_PATH[-1]}
# DOCKER_ID_USER is your docker ID and should be defined at linux session startup
export ENVIRONMENT_ROOT_NAME=${ENVIRONMENT}"_"${DOCKER_ROOT_NAME}
export IMAGE=${DOCKER_ID_USER}"/"${ENVIRONMENT_ROOT_NAME}
export CONTAINER=${DOCKER_ID_USER}"_"${ENVIRONMENT_ROOT_NAME}
export REPOSITORY=${IMAGE}
# set specific configurations
SPECIFIC_CONFIGURATION_SCRIPT=specificConfiguration.sh
LOCAL_CONFIGURATION_SCRIPT=${DIRECTORY_FULL_PATH}/${SPECIFIC_CONFIGURATION_SCRIPT}
if [[ -x ${LOCAL_CONFIGURATION_SCRIPT} ]]; then
    . ${LOCAL_CONFIGURATION_SCRIPT}
fi
ENVIRONMENT_CONFIGURATION_SCRIPT=${DIRECTORY_FULL_PATH}/${ENVIRONMENT}/${SPECIFIC_CONFIGURATION_SCRIPT}
if [[ -x ${ENVIRONMENT_CONFIGURATION_SCRIPT} ]]; then
    . ${ENVIRONMENT_CONFIGURATION_SCRIPT}
fi
