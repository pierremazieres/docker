#!/usr/bin/env bash
#set -xe
# run specific create (if needed)
SPECIFIC_CREATE_SCRIPT=${DIRECTORY_FULL_PATH}/specificCreate.sh
if [[ -x ${SPECIFIC_CREATE_SCRIPT} ]]; then
    ${SPECIFIC_CREATE_SCRIPT}
fi
