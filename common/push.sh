#!/usr/bin/env bash
#set -xe
# connect to docker hub, push to repository & disconnect from docker hub
# DOCKER_ID_USER & DOCKER_ID_PASSWORD are your docker IDs and should be defined at linux session startup
docker login -u ${DOCKER_ID_USER} -p ${DOCKER_ID_PASSWORD}
docker push ${REPOSITORY}
docker logout
