#!/usr/bin/env bash
#set -xe
# display image & container status
docker images ${IMAGE}
docker ps -a -f "name="${CONTAINER}
