#!/usr/bin/env bash
#set -xe
# WRITE SPECIFIC BUILD INSTRUCTIONS HERE
mkdir -p ${ENVIRONMENT}/log/apache2
# ask user for settings
echo -n "user login : " && read USER_LOGIN
echo -n "user password : " && read USER_PASSWORD
# apply user settings to Dockerfile
sed -i s/"{USER_LOGIN}"/${USER_LOGIN}/g Dockerfile
sed -i s/"{USER_PASSWORD}"/${USER_PASSWORD}/g Dockerfile
