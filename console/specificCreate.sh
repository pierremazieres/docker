#!/usr/bin/env bash
#set -xe
# WRITE SPECIFIC CREATE INSTRUCTIONS HERE
# sample files can be found in archive : sample.tar.gz
# they contains folders :
# - configuration
ENVIRONMENT_DIRECTORY_FULL_PATH=${DIRECTORY_FULL_PATH}/${ENVIRONMENT}
docker create \
    --name ${CONTAINER} \
    -v ${ENVIRONMENT_DIRECTORY_FULL_PATH}/configuration/ssh:/etc/ssh \
    -v ${ENVIRONMENT_DIRECTORY_FULL_PATH}/configuration/zsh:/etc/zsh \
    -v ${ENVIRONMENT_DIRECTORY_FULL_PATH}/configuration/apache2:/etc/apache2 \
    -v ${ENVIRONMENT_DIRECTORY_FULL_PATH}/html:/var/www/html \
    -v ${ENVIRONMENT_DIRECTORY_FULL_PATH}/log:/var/log \
    -p ${SSH_PORT}:22 \
    -p ${HTTP_PORT}:80 \
    -h ${ENVIRONMENT_ROOT_NAME} \
    ${IMAGE}
