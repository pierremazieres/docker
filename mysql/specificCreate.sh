#!/usr/bin/env bash
#set -xe
# WRITE SPECIFIC CREATE INSTRUCTIONS HERE
# sample files can be found in archive : sample.tar.gz
# they contains folders :
# - configuration : default configuration
# - data : default databases
# log : will be created if not existing
ENVIRONMENT_DIRECTORY_FULL_PATH=${DIRECTORY_FULL_PATH}/${ENVIRONMENT}
docker create \
    --name ${CONTAINER} \
    -v ${ENVIRONMENT_DIRECTORY_FULL_PATH}/configuration:/etc/mysql \
    -v ${ENVIRONMENT_DIRECTORY_FULL_PATH}/data:/var/lib/mysql \
    -v ${ENVIRONMENT_DIRECTORY_FULL_PATH}/log:/var/log/mysql \
    -p ${MYSQL_PORT}:3306 \
    ${IMAGE}
# set fine container directory rights
docker start ${CONTAINER}
docker exec -it ${CONTAINER} /bin/bash -c "chown -R mysql:adm /var/log/mysql && chown -R mysql:mysql /var/lib/mysql"
docker stop ${CONTAINER}
