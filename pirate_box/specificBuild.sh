#!/usr/bin/env bash
#set -xe
# WRITE SPECIFIC BUILD INSTRUCTIONS HERE
# ask user for settings
echo "forum settings"
echo -n "admin password : " && read ADMIN_PASSWORD
echo -n "secret : " && read SECRET
# apply user settings to configuration
cp -R script.pattern script
sed -i s/"{ADMIN_PASSWORD}"/${ADMIN_PASSWORD}/g script/config.pl
sed -i s/"{SECRET}"/${SECRET}/g script/config.pl
