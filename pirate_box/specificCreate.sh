#!/usr/bin/env bash
#set -xe
# WRITE SPECIFIC CREATE INSTRUCTIONS HERE
docker create \
    --name ${CONTAINER} \
    -p 80:80 \
    -p 8080:8080 \
    ${IMAGE}
