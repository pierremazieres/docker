#!/usr/bin/env bash
#set -xe
# load configuration
. ../common/configuration.sh $(readlink -f $0) $1
# create container
../common/create.sh
