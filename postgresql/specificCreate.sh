#!/usr/bin/env bash
#set -xe
# WRITE SPECIFIC CREATE INSTRUCTIONS HERE
# sample files can be found in archive : sample.tar.gz
# they contains folders :
# - configuration : default configuration
# - data : default databases
# log : will be created if not existing
ENVIRONMENT_DIRECTORY_FULL_PATH=${DIRECTORY_FULL_PATH}/${ENVIRONMENT}
docker create \
    --name ${CONTAINER} \
    -v ${ENVIRONMENT_DIRECTORY_FULL_PATH}/configuration:/etc/postgresql \
    -v ${ENVIRONMENT_DIRECTORY_FULL_PATH}/data:/var/lib/postgresql \
    -v ${ENVIRONMENT_DIRECTORY_FULL_PATH}/log:/var/log/postgresql \
    -p ${POSTGRESQL_PORT}:5432 \
    ${IMAGE}
# set fine container directory rights
docker start ${CONTAINER}
docker exec -it ${CONTAINER} /bin/bash -c "chown -R postgres:postgres /etc/postgresql ; chown -R postgres:adm /var/log/postgresql ; chown -R postgres:postgres /var/lib/postgresql"
docker stop ${CONTAINER}
