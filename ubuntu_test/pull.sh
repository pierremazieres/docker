#!/usr/bin/env bash
#set -xe
# load configuration
. ../common/configuration.sh $(readlink -f $0) $1
# pull from docker hub
../common/pull.sh
