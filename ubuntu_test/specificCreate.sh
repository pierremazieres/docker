#!/usr/bin/env bash
#set -xe
# WRITE SPECIFIC CREATE INSTRUCTIONS HERE
docker create \
    --name ${CONTAINER} \
    -v ${DIRECTORY_FULL_PATH}/${ENVIRONMENT}/shared:/home/shared \
    ${IMAGE}
